//
//  ViewController.m
//  convert tableData to propertyList
//
//  Created by OSX on 16/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *showRecords;
    NSMutableArray *showImages;
    UITableView *showTV;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    showRecords = [[NSArray alloc] init];
    showImages = [[NSArray alloc] init];
    
    showTV = [[UITableView alloc] initWithFrame:self.view.frame];
    showTV.delegate = self;
    showTV.dataSource = self;
    showTV.rowHeight = 70;
    [self.view addSubview:showTV];
    
    //Add a header view at the top of tableView
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    headerView.backgroundColor = [UIColor lightGrayColor];
    
    showTV.tableHeaderView = headerView;
    
    // Here get the path of the plistList
    NSString *path = [[NSBundle mainBundle] pathForResource:@"SocialNetworkingList" ofType:@"plist"];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    showRecords = [[dict objectForKey:@"showRecords"] mutableCopy];
    showImages = [[dict objectForKey:@"showImages"] mutableCopy];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return showRecords.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"recordsTVCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [showRecords objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:[showImages objectAtIndex:indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [showRecords removeObjectAtIndex:indexPath.row];
    [showImages removeObjectAtIndex:indexPath.row];
    [showTV reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
